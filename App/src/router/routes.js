// ╔═╗┌┬┐┌┬┐┌─┐┌─┐  ╔╦╗┌─┐┌─┐┬┌─┐┬
// ║╣ ││││││├─┤└─┐  ║║║├─┤│ ┬││  │
// ╚═╝┴ ┴┴ ┴┴ ┴└─┘  ╩ ╩┴ ┴└─┘┴└─┘o
const matchRoute = page => import(`pages/${page}.vue`);

const routes = [
  // Auth//x
  {
    path: "/auth",
    component: () => import("layouts/AuthLayout.vue"),
    meta: {
      requiresAuth: false
    },
    children: [
      { path: "/login", component: () => import("pages/Auth/Login") },
      { path: "/signup", component: () => import("pages/Auth/Register") },
      { path: "/logout", component: () => import("pages/Auth/Logout") },
      { path: "/intro", name: "Intro", component: () => import("pages/Intro") },
      {
        path: "/forgot-password",
        component: () => import("pages/Auth/ForgotPassword")
      }
    ]
  },
  {
    path: "/layout",
    component: () => import("layouts/MainLayout.vue"),
    meta: {
      requiresAuth: true
    },
    children: [
      {
        path: "/create-plan",
        name: "CreatePlan",
        component: () => import("pages/CreatePlan")
      },
      {
        path: "/settings",
        name: "Settings",
        component: () => import("pages/Profile/Index")
      },
      {
        path: "/update-info",
        name: "UpdateAccountInfo",
        component: () => import("pages/Settings/UpdateAccountInfo")
      },
      {
        path: "/update-bank-info",
        name: "UpdateBankInfo",
        component: () => import("pages/Settings/UpdateBankInfo")
      },
      {
        path: "/withdraw",
        name: "Withdraw",
        component: () => import("pages/Withdraw")
      },
      {
        path: "/plan/:planID",
        name: "ViewPlan",
        component: () => import("pages/ViewPlan")
      },
      {
        path: "/edit-plan/:planID",
        name: "EditPlan",
        component: () => import("pages/EditPlan")
      },
      {
        path: "/quick-save",
        name: "QuickSave",
        component: () => import("pages/QuickSave")
      }
    ]
  },
  {
    path: "",
    component: () => import("layouts/FlatLayout.vue"),
    meta: {
      requiresAuth: true
    },
    children: [
      { path: "/", name: "Home", component: () => import("pages/Home") },
      { path: "/plans", name: "Plans", component: () => import("pages/Plans") },
      {
        path: "/profile",
        name: "Profile",
        component: () => import("pages/Profile/Index")
      }
    ]
  }
];

// Always leave this as last one
if (process.env.MODE !== "ssr") {
  routes.push({
    path: "*",
    component: () => import("pages/Errors/404.vue")
  });
}

export default routes;
