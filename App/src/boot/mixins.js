import Vue from "vue";
import { Cookies } from "quasar";
import { FingerprintAIO } from "@ionic-native/fingerprint-aio";

Vue.mixin({
  data() {
    return {
      tab: Cookies.get("currentTab") ?? "Home"
    };
  },
  methods: {
    ensureFingerprint() {
      FingerprintAIO.show({
        title: "Verify fingerprint"
      }).catch(e => {
        // only bypass this auth when vue is running on development
        if (!process.env.DEV) {
          this.$router.back();
        }
      });
    }
  },
  watch: {
    tab(val) {
      // save the current tab so that the component will ..
      // .. remember every page that it went out from
      Cookies.set("currentTab", val);
      // redirect to the tab
      this.$router.push({ name: val });
    }
  },
  computed: {
    // handles if app is outdated
    isAppOutdated() {
      return this.$store.state.user.isUpdateRequired;
    },
    isAuthenticated() {
      return !!Cookies.get("jwt");
    },
    user() {
      return this.$store.state.user.user;
    },
    navTitle() {
      return this.$store.state.navTitle;
    }
  },
  created() {
    var title = this.$options.title;
    if (title) {
      this.$store.state.navTitle = title;
    }
  }
});
