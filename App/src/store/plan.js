import { Notify } from "quasar";

const user = {
  namespaced: false,

  state: { plans: [] },
  mutations: {
    PUSH_PLAN(state, $plan) {
      state.plans.push($plan);
    },
    SET_PLANS(state, $plans) {
      state.plans = $plans;
    }
  },
  actions: {
    async GET_PLANS({ commit }) {
      let { data } = await $axios.get("/plans");
      if (data) {
        commit("SET_PLANS", data);
      }
    },

    async WITHDRAW({ commit }, $withdrawData) {
      try {
        let { data } = await $axios.post("/withdraw", $withdrawData);
        if (data) {
          Notify.create({
            message: `${$withdrawData.amount} has been withdrawn from your account`,
            color: "positive"
          });

          this.$router.back();
        }
      } catch ({ response: { status } }) {
        switch (status) {
          case 423:
            Notify.create({
              message: "This plan is locked at the moment,",
              color: "negative"
            });
            break;
          case 413:
            Notify.create({
              message:
                "Please try with a lower `amount` payload or contact support",
              color: "negative"
            });
            break;
          case 424:
            Notify.create({
              message:
                "You havent yet set your bank details, do that first and try again",
              color: "negative"
            });
            break;
          default:
            Notify.create({
              message: "Sorry, contact support, a 500 error was reported!",
              color: "negative"
            });
        }
      }
    },

    async TOGGLE_PAUSE_PLAN({ commit }, $planData) {
      let { data } = await $axios.patch("/plan", $planData);
      console.log(data);
      console.log($planData);
    },

    async UPDATE_PLAN({ state, commit, rootState }, $planData) {
      try {
        let { data } = await $axios.patch("/plan", $planData);
        if (data) {
          // performs pragmatic update on school without reloading whole page
          commit("GET_PLANS", data);
          Notify.create({
            message: `The plan has been updated successfully`,
            color: "positive"
          });
          this.$router.push({ name: "Plans" });
        }
      } catch (e) {
        console.log(e);
        Notify.create({
          message: `Sorry ! An error occured while trying to update plan`,
          color: "negative"
        });
      }
    },
    async CREATE_PLAN({ state, commit, rootState }, $planData) {
      try {
        let { data } = await $axios.put("/plan", $planData);
        if (data) {
          // performs pragmatic update on school without reloading whole page
          commit("PUSH_PLAN", data);
          Notify.create({
            message: `The plan has been created successfully`,
            color: "positive"
          });

          this.$router.push({ name: "Plans" });
        }
      } catch ({ response: { status } }) {
        Notify.create({
          message: `Sorry ! An error occured while trying to create plan`,
          color: "negative"
        });
      }
    }
  },
  getters: {
    getTotalPlanAmount(state) {
      let num = [0];
      state.plans.map(plan => num.push(plan.currentAmount));
      return num.reduce((a, b) => a + b);
    },
    getPlan: state => planId => {
      return state.plans.find(plan => plan.id == planId);
    }
  }
};

export default user;
