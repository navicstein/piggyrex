import { Cookies, Notify, Dialog } from "quasar";

const user = {
  namespaced: false,
  state: {
    user: {},
    version: "0.1",
    isUpdateRequired: false
  },
  mutations: {
    UPDATE_AUTH_STATE(state, status) {
      state.isLoggedout = status;
    },
    SET_USER(state, user) {
      Cookies.set("jwt", user.jwt);
      // update the loggedin cookie state
      Cookies.set("isLoggedin", true);
      state.user = user;
    }
  },
  actions: {
    INIT({ state, commit }) {
      return new Promise(async (resolve, reject) => {
        try {
          let { data } = await $axios.patch("/");
          if (data) {
            resolve(data);

            commit("SET_USER", data.user);
            commit("SET_PLANS", data.plans);

            //  this handles the version notification
            if (data.version !== state.version) {
              state.isUpdateRequired = true;
            }
          }
        } catch ({ response }) {
          reject(response);
        }
      });
    },

    async UPDATE_USER({ state, commit, rootState }, $userData) {
      try {
        let { data } = await $axios.put("/account/update-profile", $userData);
        if (data) {
          commit("SET_USER", data);
          this.$router.back();
        }
      } catch ({ response }) {
        Notify.create({
          color: "negative",
          message: "Sorry, an unexpected error occured, please try again later"
        });
      }
    },

    async EDIT_BANK_DETAILS({ commit }, $userData) {
      let { data } = await $axios.patch("/account/bank", $userData);
      if (data) {
        commit("SET_USER", data);
        this.$router.back();
      }
    },

    async LOGIN({ commit }, $data) {
      let { identifier, password } = $data;
      try {
        let { data } = await $axios.put("/entrance/login", {
          identifier,
          password
        });
        if (data) {
          commit("SET_USER", data);
          // this.$router.push({ name: "Home" });
          window.location.href = "/";
        }
      } catch (error) {
        if (error.response?.status === 401) {
          Notify.create({
            color: "red",
            message: "Invalid identifier or password"
          });
        } else {
          Notify.create({
            color: "red",
            message: "Unexpected error occured"
          });
        }
      }
    },

    async REGISTER({ state, commit, rootState }, $data) {
      let { emailAddress, password, phoneNumber, fullName } = $data;
      try {
        let { data } = await $axios.post("/entrance/signup", {
          emailAddress,
          password,
          phone: phoneNumber,
          fullName
        });

        if (data) {
          // commit user
          commit("SET_USER", data);
          // redirect to create plan
          // this.$router.push({ name: "CreatePlan" });
          window.location.href = "/#/create-plan";
        }
      } catch ({ response: { status } }) {
        switch (status) {
          case 409:
            Dialog.create({
              title: "Inconsitency Violation!",
              message:
                "Either your email or password already exist, please try again with different details"
            });
          default:
            Notify.create({
              color: "red",
              message: "Sorry, an unexpected error occured"
            });
        }
      }
    }
  },
  getters: {
    user(state) {
      return state.user;
    }
  }
};

export default user;
