import Vue from "vue";
import Vuex from "vuex";
import VuexPersistence from "vuex-persist";

import user from "./user";
import plan from "./plan";

Vue.use(Vuex);

/*
 * If not building with SSR mode, you can
 * directly export the Store instantiation;
 *
 * The function below can be async too; either use
 * async/await or return a Promise which resolves
 * with the Store instance.
 */
const vuexLocal = new VuexPersistence({
  storage: window.localStorage
});

export default function(/* { ssrContext } */) {
  const Store = new Vuex.Store({
    modules: {
      user,
      plan
    },
    state: {
      navTitle: ""
    },
    strict: false,
    plugins: [vuexLocal.plugin]
  });

  return Store;
}
