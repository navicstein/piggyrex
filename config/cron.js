// ['seconds', 'minutes', 'hours', 'dayOfMonth', 'month', 'dayOfWeek']
// schedule: '30 47 15 17 may *',
// in May 17 15:47:30 GMT-0300 (BRT)
const moment = require("moment");
const PayStack = require("paystack-node");

module.exports.cron = {
  myFirstJob: {
    schedule: "0 0 * * *",
    async onTick() {
      const APIKEY = sails.config.custom.PAYSTACK_APIKEY;
      const paystack = new PayStack(APIKEY, sails.config.environment);
      let todaysDate = moment(Date.now()).format("YYYY/MM/DD");

      // find all plans that thier `endDate` is currently today
      // prettier-ignore
      let plans = await Plan.find().where({ endDate: todaysDate, isPaused: false }).populate("user")

      sails.log.silly("Todays date:", todaysDate);

      plans.forEach(async plan => {
        sails.log.warn("Canceling active subscription for:", plan.title);
        let pauseData = {
          code: plan.subCode,
          token: plan.emailToken
        };
        // Update the savings in paystack
        await paystack.disableSubscription(pauseData).then(async () => {
          // ... the disable the plan in our DB
          await Plan.updateOne({
            id: plan.id
          }).set({ isPaused: true });
        });
        // Credit back the whole `currentAmount` to the user of the plan
        await sails.helpers.makeWithdraw.with({
          amount: plan.currentAmount * 100,
          planID: plan.id
        });

        // TODO: Send an email to the user
      });
    },
    onComplete() {
      console.log("I am triggering when job is complete");
    }
  }
};
