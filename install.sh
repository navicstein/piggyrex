#!/bin/bash

# Author: Navicstein Rotciv
# Email: Navicstein@gmail.com
echo "  - Author: Navicstein Rotciv"
echo "  - Email: Navicstein@gmail.com"
echo "  - Company: https://crudox.com/navicstein"
echo "  - -------------------------------------------------------------------------"
echo "  - This script assumes a dead-simple, opinionated setup on Heroku."
echo "  - But, of course, you can deploy your app anywhere you like."
echo "  - (Node.js/Sails.js apps are supported on all modern hosting platforms.)"
echo "  - Warning: Specifically, this script assumes you are on the master branch,"
echo "  - and that your app can be deployed simply by force-pushing on top of the *deploy* branch."
echo "  - It will also temporarily use a local *predeploy* branch for preparing assets,"
echo "  - that it will delete after it finishes."
echo "  - Please make sure there is nothing you care about on either of these two branches!!!"

echo '  - Alright, here we go. trigerring auto deploy .. ..' && echo

echo ' ------------------------- ' && echo
echo ' 
    ██████╗  ██████╗  ██████╗ ████████╗██╗███╗   ██╗ ██████╗          
    ██╔══██╗██╔═══██╗██╔═══██╗╚══██╔══╝██║████╗  ██║██╔════╝          
    ██████╔╝██║   ██║██║   ██║   ██║   ██║██╔██╗ ██║██║  ███╗         
    ██╔══██╗██║   ██║██║   ██║   ██║   ██║██║╚██╗██║██║   ██║         
    ██████╔╝╚██████╔╝╚██████╔╝   ██║   ██║██║ ╚████║╚██████╔╝██╗██╗██╗
    ╚═════╝  ╚═════╝  ╚═════╝    ╚═╝   ╚═╝╚═╝  ╚═══╝ ╚═════╝ ╚═╝╚═╝╚═╝
--------------------------- ' && echo

setupServer() {
    echo '
    ╦┌┐┌┌─┐┌┬┐┌─┐┬  ┬  ┬┌┐┌┌─┐  ╔═╗┌─┐┬─┐┬  ┬┌─┐┬─┐  ┌┬┐┌─┐┌─┐┌─┐┌┐┌┌┬┐┌─┐┌┐┌┌─┐┬┌─┐┌─┐  
    ║│││└─┐ │ ├─┤│  │  │││││ ┬  ╚═╗├┤ ├┬┘└┐┌┘├┤ ├┬┘   ││├┤ ├─┘├┤ │││ ││├┤ ││││  │├┤ └─┐  
    ╩┘└┘└─┘ ┴ ┴ ┴┴─┘┴─┘┴┘└┘└─┘  ╚═╝└─┘┴└─ └┘ └─┘┴└─  ─┴┘└─┘┴  └─┘┘└┘─┴┘└─┘┘└┘└─┘┴└─┘└─┘oo
    ' && echo
    cd ./Server
    npm install
    cd ..
}

setupServer

