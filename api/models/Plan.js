/**
 * Saving.js
 *
 * @description :: A model definition represents a database table/collection.
 * @docs        :: https://sailsjs.com/docs/concepts/models-and-orm/models
 */

module.exports = {
  attributes: {
    //  ╔═╗╦═╗╦╔╦╗╦╔╦╗╦╦  ╦╔═╗╔═╗
    //  ╠═╝╠╦╝║║║║║ ║ ║╚╗╔╝║╣ ╚═╗
    //  ╩  ╩╚═╩╩ ╩╩ ╩ ╩ ╚╝ ╚═╝╚═╝
    title: {
      type: "string",
      required: true
    },
    planCode: {
      type: "string",
      required: false,
      description: "plancode from paystack"
    },
    // planId: {
    //   type: "string",
    //   required: false,
    //   description: "plan_id from paystack"
    // },
    hasActiveSubscription: {
      type: "boolean",
      required: false,
      defaultsTo: false,
      description: "If the user has subscribed to the plan created"
    },
    // subId: {
    //   type: "string",
    //   required: false,
    //   description: "subscription id from paystack"
    // },
    subCode: {
      type: "string",
      required: false,
      description: "subscription `code` from paystack"
    },
    emailToken: {
      type: "string",
      required: false,
      description:
        "`email_token` code from paystack used to `enable` or `disable` a subscription"
    },
    authorizationUrl: {
      type: "string",
      required: false,
      description: "Auth url from paystack"
    },
    targetAmount: {
      type: "number",
      required: true
    },
    targetAmountInKobo: {
      type: "number",
      required: false
    },
    interval: {
      type: "string",
      isIn: ["daily", "weekly", "monthly", "manually"],
      defaultsTo: "weekly"
    },
    amountToSave: {
      type: "number",
      defaultsTo: 0,
      description: "The amount to be saving per interval"
    },
    isPaused: {
      type: "boolean",
      defaultsTo: true
    },
    // startDate: {
    //   type: "string"
    // },
    endDate: {
      type: "string"
    },
    nextPaymentDate: {
      type: "string"
    },
    lastChargedDate: {
      type: "string",
      description: "The last time this account was credited and charged",
      extendedDescription:
        "The last time paystack debeited the user subscription"
    },
    currentAmount: {
      type: "number",
      defaultsTo: 0,
      description: "The amount currently in the users plan"
    },
    //  ╔═╗╔╦╗╔╗ ╔═╗╔╦╗╔═╗
    //  ║╣ ║║║╠╩╗║╣  ║║╚═╗
    //  ╚═╝╩ ╩╚═╝╚═╝═╩╝╚═╝

    //  ╔═╗╔═╗╔═╗╔═╗╔═╗╦╔═╗╔╦╗╦╔═╗╔╗╔╔═╗
    //  ╠═╣╚═╗╚═╗║ ║║  ║╠═╣ ║ ║║ ║║║║╚═╗
    //  ╩ ╩╚═╝╚═╝╚═╝╚═╝╩╩ ╩ ╩ ╩╚═╝╝╚╝╚═╝
    user: {
      model: "user"
    },

    withdraws: {
      via: "plan",
      collection: "withdraw"
    }
  }
};
