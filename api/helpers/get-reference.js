module.exports = {
  friendlyName: "Get reference",

  description: "",

  inputs: {
    len: {
      type: "number",
      defaultsTo: 5
    },
    upper: {
      type: "boolean",
      defaultsTo: false
    }
  },

  exits: {
    success: {
      outputFriendlyName: "Reference"
    }
  },

  fn: async function({ len, upper }) {
    // Get reference.
    //you can put any unique reference implementation code here
    let text = "";
    let possible = "abcdefghijklmnopqrstuvwxyz0123456789";
    if (upper) {
      possible.concat("ABCDEFGHIJKLMNOPQRSTUVWXYZ");
    }

    for (let i = 0; i < len; i++) {
      text += possible.charAt(Math.floor(Math.random() * possible.length));
    }

    // Send back the result through the success exit.
    return text;
  }
};
