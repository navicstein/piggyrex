module.exports = {
  friendlyName: "Make withdraw",

  description: "",

  inputs: {
    planID: {
      type: "string",
      required: true
    },
    amount: {
      type: "number",
      required: true
    }
  },

  exits: {
    bankNotFound: {
      description: "Users bank not set"
    },
    payloadTooHigh: {
      descript: "Payload is too high, try with a lower amount"
    },
    locked: {
      description: "Plan is locked dues to some reasons"
    },
    error: {
      description: "Sorry something went wrong"
    },
    success: {
      description: "All done, the withdraw has been triggered"
    }
  },

  fn: async function(inputs) {
    let { amount, planID } = inputs;

    const moment = require("moment");
    const PayStack = require("paystack-node");

    const APIKEY = sails.config.custom.PAYSTACK_APIKEY;
    const paystack = new PayStack(APIKEY, sails.config.environment);

    let todaysDate = moment(Date.now()).format("YYYY/MM/DD");

    let plan = await Plan.findOne({ id: planID }).populate("user");

    if (!plan) throw "notfound";

    // prettier-ignore
    let { user: { bank }, user } = plan;

    if (!bank) throw "bankNotFound";

    const toKobo = (naira = 0) => parseInt(naira * 100);

    // prettier-ignore
    const randDescription = `Withdrawing ${amount} from ${plan.title.toUpperCase()}: ${user.bank.label}`;

    if (plan.currentAmount < amount) throw "payloadTooHigh";

    sails.log.silly(plan.endDate, todaysDate);

    // finally  ..
    try {
      // first create a transfer reciept
      var {
        body: { data }
      } = await paystack.createTransferRecipient({
        name: user.fullName,
        description: randDescription,
        account_number: user.accountNumber,
        bank_code: user.bank.value
      });

      let transferData = data;
      if (data) {
        let awayData = {
          reason: randDescription,
          amount: toKobo(amount),
          recipient: transferData.recipient_code,
          // prettier-ignore
          reference: plan.id + "-" + (await sails.helpers.getReference.with({ len: 5 }))
        };

        var {
          body: { data }
        } = await paystack.initiateTransfer(awayData);
      }
      sails.log.silly(data);
      return plan;
    } catch (e) {
      sails.log.error(e);
      throw "error";
    }
  }
};
