module.exports = {
  friendlyName: "Issue jwt",

  description: "",

  inputs: {
    userId: {
      type: "string",
      required: true
    }
  },

  exits: {
    success: {
      description: "All done."
    }
  },

  fn: async function({ userId }) {
    let jwt = require("jsonwebtoken");
    let tokenSecret = sails.config.custom.JWT_SECRET;
    // Token Secret that we sign it with
    const signedJWT = jwt.sign({ id: userId }, tokenSecret);

    return signedJWT;
  }
};
