module.exports = {
  friendlyName: "Init",

  description:
    "Initialize the user, returning his, savings, profile & withdraws.",

  inputs: {},

  exits: {
    success: {
      statusCode: 201,
      description: "The whole users data has been sent out to the user-agent",
      extendedDescription:
        "The user has requested his data, this is the same as the user hitting the init route in the home page",
    },
  },

  fn: async function (inputs) {
    var user = await User.findOne({ id: this.req.me.id });
    var plans = await Plan.find({ user: user.id }).populate("withdraws");
    // All done.
    return { user, plans, version: "0.2" };
  },
};
