/**
 * PsWebhookController
 *
 * @description :: Server-side actions for handling incoming requests.
 * @help        :: See https://sailsjs.com/docs/concepts/actions
 */
const toNaira = (kobo = 0) => parseInt(kobo / 100);

module.exports = {
  /**
   * `PsWebhookController.hook()`
   */
  hook: async function (req, res) {
    const EVENT = req.body ? req.body.event : null;

    sails.log.info("WEBHOOK_EVENT: ", EVENT);
    sails.log.info("- - - ".repeat(20));
    sails.log.info(req.body);
    sails.log.info("- - - ".repeat(20));

    if (EVENT == "subscription.create") {
      //* Objects
      let {
        data: { customer, authorization, plan },
      } = req.body;
      //* Strings && Numbers
      let {
        data: { amount, subscription_code, email_token, next_payment_date },
      } = req.body;

      amount = toNaira(amount);

      try {
        // look up the savings plan with the paystacks `plan_code`
        plan = await Plan.findOne({
          planCode: plan.plan_code,
        });

        // sails.log.info("Plan", plan);

        if (plan && plan.id) {
          plan = await Plan.updateOne({ id: plan.id }).set({
            hasActiveSubscription: true, // enable the active sub
            isPaused: false, // unpause the savings plan
            subCode: subscription_code, // for future reference
            nextPaymentDate: next_payment_date, // next payment date
            emailToken: email_token,
            // clear out any outstanding `authUrl or whatever`, the user has already been subscribed
            authorizationUrl: "",
          });
        }
        // find the user
        user = await User.findOne({
          emailAddress: customer.email,
        });
        // if (user && user.cards) {
        //   // create a card for the user incase he did not enter it manually
        //   if (user.cards.length === 0) {
        //     await strapi.query("card").create({
        //       expiry: authorization.exp_month + "/" + authorization.exp_year,
        //       cardType: authorization.card_type,
        //       bank: authorization.bank,
        //       brand: authorization.brand,
        //       nameOnCard: user.fullName,
        //       cardNumber: "xxx xxx xxx " + authorization.last4,
        //       bank: authorization.bank,
        //       user: user.id
        //     });
        //   }
        // }
      } catch (e) {
        throw e;
      }
    } else if (EVENT == "transfer.success") {
      let {
        data: { amount, reference, status, reason, transfer_code },
      } = req.body;

      // convert the amount to `naira`
      amount = toNaira(amount);

      // the reference is a combination of the `savings` ID and a random number of `5` len
      let planCode = reference.split("-")[0];

      let __plan = await Plan.findOne({ planCode }).populate("user");

      if (__plan) {
        // create the withdraw history, and update the status
        await Withdraw.create({
          plan: __plan.id,
          user: __plan.user.id,
          transferCode: transfer_code,
          isResolved: true,
          amount,
          status,
          reason,
        });

        let currentDeductedPlanAmount = _plan.currentAmount - amount;

        // .. update the savings plan `currentAmount`
        await Plan.updateOne({ id: __plan.id }).set({
          currentAmount: currentDeductedPlanAmount,
        });
      }

      // this is the first hook called before subscription
    } else if (EVENT == "charge.success") {
      let {
        // <objects>
        plan,
        customer,
        // </objects>
        amount,
        paid_at,
        ip_address,
        reference,
      } = req.body.data;

      // run this code anytime paystack charges the user
      amount = toNaira(amount);

      // find the plan and update the `currentAmount`
      let _plan = await Plan.findOne({
        planCode: plan.plan_code,
      });

      // await User.updateOne({
      //   customerCode: customer.customer_code
      // }).set({
      //   tosAcceptedByIp: ip_address
      // });

      if (_plan) {
        let upgradedAmount = _plan.currentAmount + amount; // amount in naira;

        let valuesToSet = {
          currentAmount: upgradedAmount,
          hasActiveSubscription: true,
          lastChargedDate: paid_at,
        };

        // prettier-ignore
        _plan = await Plan.updateOne({ planCode: _plan.planCode }).set(valuesToSet);
      }
    }

    return res.sendStatus(200);
  },
};
