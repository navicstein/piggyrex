module.exports = {
  friendlyName: "Create withdraw",

  description: "",

  inputs: {
    amount: {
      type: "number",
      description: "The amount in naira[number]",
      required: true
    },
    planID: {
      type: "string",
      required: true
    }
  },

  exits: {
    bankNotFound: {
      descript: "Users bank not set",
      statusCode: 424
    },
    payloadTooHigh: {
      descript: "Payload is too high, try with a lower amount",
      extendedDescription:
        "The user must have passed an illegal amount from the UA",
      statusCode: 413
    },
    locked: {
      descript: "Plan is locked dues to some reasons",
      extendedDescription: "No active subscription for the user!",
      statusCode: 423
    }
  },

  fn: async function({ amount, planID }) {
    // just compute the discount and trigger the helper
    let _discount = 100 / 2; // pecentage payload
    await sails.helpers.makeWithdraw
      .with({
        amount: amount - _discount,
        planID
      })
      .intercept("payloadTooHigh", "payloadTooHigh")
      .intercept("locked", "locked")
      .intercept("bankNotFound", "bankNotFound")
      .retry();
  }
};
