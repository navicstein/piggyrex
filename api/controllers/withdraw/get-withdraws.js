module.exports = {
  friendlyName: "Get withdraws",

  description: "",

  inputs: {},

  exits: {},

  fn: async function(inputs) {
    let withdraws = await Withdraw.find({
      user: this.req.me.id
    }).populate("plan");
    // All done.
    return withdraws;
  }
};
