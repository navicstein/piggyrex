const toKobo = (naira = 0) => parseInt(naira * 100);
const toNaira = (kobo = 0) => parseInt(kobo / 100);

module.exports = {
  friendlyName: "Create plan",

  description: "",

  inputs: {
    title: {
      type: "string",
      required: true
    },
    amountToSave: {
      type: "number",
      defaultsTo: 0,
      description: "The amount to be saving per interval"
    },
    endDate: {
      required: true,
      type: "string"
    },
    targetAmount: {
      required: true,
      type: "string"
    },
    interval: {
      type: "string",
      isIn: ["daily", "weekly", "monthly", "manually"],
      required: true
    }
  },

  exits: {
    locked: {
      statusCode: 423,
      description: "Users MAX_PLAN reached!",
      extendedDescription: "The user needs to upgrade his plan"
    }
  },

  fn: async function(inputs) {
    let PayStack = require("paystack-node");
    let APIKEY = sails.config.custom.PAYSTACK_APIKEY;
    const paystack = new PayStack(APIKEY, sails.config.environment);

    let user = this.req.me;
    let plans = await Plan.find({
      user: user.id
    });

    // The user is limited to 4 savings account alone!
    if (plans.length > 3) {
      throw "locked";
    }

    // Attempt to create our local plan if not exist
    let plan = await Plan.create(
      Object.assign(inputs, {
        user: user.id
      })
    ).fetch();

    let { amountToSave, title, interval, targetAmount, endDate } = inputs;

    try {
      // attempt to create paystacks' plan first
      let { body } = await paystack.createPlan({
        name: title,
        amount: toKobo(amountToSave),
        send_invoices: true,
        interval: interval.toLowerCase()
      });

      if (body) {
        // get the authoriaztion signature for this `plan`
        var init = await paystack.initializeTransaction({
          amount: toKobo(targetAmount),
          email: user.emailAddress,
          reference: await sails.helpers.getReference(),
          plan: body.data.plan_code
        });
        if (init.body) {
          // Attempt to update the savings after paysatck must have `passed` the transaction..
          // .. this is handy in case paystack did'nt dropin successfully, the saving can be ..
          // .. updated later
          plan = await Plan.updateOne({ id: plan.id }).set(
            Object.assign(inputs, {
              targetAmountInKobo: toKobo(targetAmount),
              authorizationUrl: init.body.data.authorization_url,
              planCode: body.data.plan_code,
              hasActiveSubscription: false // denotes if the user has an active subscription for this plan
            })
          );
        }
      }
    } catch (e) {
      console.log("-".repeat(40));
      console.log(e);
      console.log("-".repeat(40));
      // if anything goes wrong .. revert and delete our locally created saving
      await Plan.destroyOne({ id: plan.id });
    }
    // All done.
    return plan;
  }
};
