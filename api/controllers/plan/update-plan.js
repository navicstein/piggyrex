module.exports = {
  friendlyName: "Update plan",

  description: "",

  inputs: {
    planID: {
      type: "string",
      required: true
    },
    title: {
      type: "string",
      required: false
    },
    amountToSave: {
      type: "number",
      description: "The amount to be saving per interval"
    },
    endDate: {
      required: false,
      type: "string"
    },
    isPaused: {
      required: false,
      type: "boolean"
    },
    interval: {
      type: "string",
      isIn: ["daily", "weekly", "monthly", "manually"]
    }
  },

  exits: {},

  fn: async function(inputs) {
    let PayStack = require("paystack-node");
    let APIKEY = sails.config.custom.PAYSTACK_APIKEY;
    const paystack = new PayStack(APIKEY, sails.config.environment);

    const toKobo = (naira = 0) => parseInt(naira * 100);

    // look up plan first
    let plan = await Plan.findOne({ id: inputs.planID });

    if (plan) {
      // Overwrite the default `plan` with the updated `plan`
      // This replaces the old values with the newer one incase some attributes were ..
      // .. missing in the incoming request
      plan = Object.assign(plan, inputs);
      try {
        let pendingData = {
          id_or_plan_code: plan.planCode || plan.planCode,
          name: plan.title,
          interval: plan.interval.toLowerCase(),
          amount: toKobo(plan.amountToSave)
        };
        // console.log(pendingData);
        // update savings plan to paystack first
        let { body } = await paystack.updatePlan(pendingData);
        //  if there's a pause $event, pause the subscription on paystack first
        // but check if there's a subscription code there
        let { isPaused } = inputs;
        try {
          if (plan.subCode && typeof isPaused !== "undefined") {
            let pauseData = {
              code: plan.subCode,
              token: plan.emailToken
            };
            isPaused === true
              ? await paystack.disableSubscription(pauseData)
              : await paystack.enableSubscription(pauseData);
          }
        } catch (e) {
          console.log("ERROR ON PAUSE_SUB", e);
        }
        // if successfull on `updatePlan`
        if (body.status) {
          plan = await Plan.updateOne({ id: inputs.planID }).set({ ...inputs });
          return plan;
        }
      } catch (e) {
        console.log(e);
        return e;
      }
    }

    // All done.
    return;
  }
};
