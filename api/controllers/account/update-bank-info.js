module.exports = {
  friendlyName: "Update bank info",

  description: "",

  inputs: {
    bank: {
      type: "json",
      required: true
    },

    accountNumber: {
      required: true,
      type: "number"
    }
  },

  exits: {},

  fn: async function(inputs) {
    let PayStack = require("paystack-node");
    let APIKEY = sails.config.custom.PAYSTACK_APIKEY;
    const paystack = new PayStack(APIKEY, sails.config.environment);

    let user = _.extend(this.req.me, inputs);

    let [first_name, last_name] = user.fullName.split(" ");

    let payStackCustomerData = {
      customer_id: user.customerCode,
      email: user.emailAddress,
      phone: user.phone,
      first_name,
      last_name
    };

    sails.log.silly(user);

    // await paystack.updateCustomer(payStackCustomerData);
    let {
      body: { data }
    } = await paystack.resolveAccountNumber({
      account_number: String(user.accountNumber),
      bank_code: user.bank.value
    });

    // merge the opts together with paystack data
    let updatingUserData = Object.assign(inputs, {
      fullName: data.account_name,
      accountNumber: data.account_number
    });

    user = await User.updateOne({ id: user.id }).set(
      _.extend(inputs, updatingUserData)
    );
    // All done.
    return user;
  }
};
